// Опишіть, як можна створити новий HTML тег на сторінці.
// createElement('elem')
// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// insertAdjacentHTML - дает возможность вставить большой код разметки хтмл, так же принимает параметры beforebegin, afterbegin, beforeend, afterend
// Як можна видалити елемент зі сторінки?
// elem.remove()

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

let array = [
  "Kharkiv",
  "Kiev",
  ["Borispol", ["New York", "Sidney"], "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];
let body = document.querySelector("body");
let newArr = [];

function showArr(arr, parent) {
  function getCloneArr(arr) {
    for (let item of arr) {
      if (typeof item !== "object") {
        newArr.push(item);
      } else {
        item.push(getCloneArr(item));
      }
    }
    return newArr;
  }
  getCloneArr(arr);

  let ul = document.createElement("ul");
  parent.append(ul);

  newArr.map((item) => {
    let li = document.createElement("li");
    li.textContent = item;
    ul.append(li);
  });
}

showArr(array, body);
